[MaskTable]
keyType = NotDefined
productName = A2iA CheckReader
productVersion = 9.0
productRelease = 1

[MaskTable.defaultDefinition]
index = 0
imageName = .\Images\*.*
label = 

[MaskTable.defaultDefinition.doc]
verbose = No
preprocessOnly = No
documentType = Check

[MaskTable.defaultDefinition.doc.verboseDetails]
preprocessedImage = Yes
extractedImages = No
characterResults = No
originalWords = No

[MaskTable.defaultDefinition.doc.imagePreprocessing]
extractIRD = No
TopClean = No
LeftClean = No
RightClean = No
BottomClean = No
reverseValues = No
skewCorrection = Yes
horizontalStreakRemoval = No

[MaskTable.defaultDefinition.doc.image]
imageOrientation = NotDefined
imageSourceType = NotDefined
inputFormat = NotDefined
transportModel = NotDefined
resolution_Dpi = 200

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check]
country = US
currency = USD
writeType = NotDefined
amountRecognition = Yes
threshold = 500
larCallPolicy = LarCallThreshold
larCallThreshold = 1.000000
prnLarCallPolicy = Auto
prnLarCallThreshold = 1.000000
printedLARCentPart = NotDefined
invalidityDetection = Yes
rearInvalidityDetection = No
Optimisation = OptimizeBinarization
InvertedText = No
endorsementCleaning = NotDefined
detectDocumentType = Yes

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields]
lar = Yes
CarLarDifferenceDetection = Yes
nameOfPayee = No
nameOfPayeeOCRType = NotDefined
payeeNameLines = NotDefined
date = Yes
definePivotYear = NotDefined
pivotYear = 0
acceptPostDated = NotDefined
address = No
payeeAddress = No
postalDictionaryPersistence = NotDefined
addressBookPersistence = NotDefined
signature = Yes
codeline = Yes
codelineFont = E13B
codelineAmount = Yes
blackPayee = NotDefined
blackPayer = NotDefined
checkNumber = Yes
accountNumber = No

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.possibleNameOfPayee]
vocabFormat = Persistent
wordProcess = NWordsPerField
keepAllAnswers = NotDefined
verification = NotDefined
aliases = NotDefined
charactersType = NotDefined

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.possibleNameOfPayee.vocabFormatInfo.CasePersistent]
vocabularyId = UserVocabularyPayee

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.possibleNameOfPayee.wordProcessInfo.CaseNWordsPerField]
maxNumberOfWords = 3
minNumberOfWords = 1

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.referenceDate]
day = 0
month = 0
year = 0

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.minimum]
day = 0
month = 0
year = 0

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.fields.maximum]
day = 0
month = 0
year = 0

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.countryInfo.CaseUS]
kind = NotDefined

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.invalidityDetails]
CAR = Yes
LAR = Yes
Signature = Yes
PayeeName = Yes
Date = Yes
Codeline = Yes
PayorsNameAndAddress = No
PayeeEndorsement = No
BankOfFirstDepositEndorsement = No
TransitEndorsement = No
Memoline = No
PayorsBank = No
considerStampAsPayeeEndorsment = No

[MaskTable.defaultDefinition.doc.documentTypeInfo.CaseCheck.check.documentTypePresence]
SaveBond = No

[MaskTable.defaultDefinition.doc.imageQuality]
enable = Yes
optimisation = MaximizeSpeed
compressedImageSize = 0

[MaskTable.defaultDefinition.doc.imageQuality.undersizeImage]
enable = Yes
minimumImageWidthThreshold = 55
minimumImageHeightThreshold = 22

[MaskTable.defaultDefinition.doc.imageQuality.foldedOrTornDocumentCorners]
enable = Yes
maximumTopLeftCornerFoldTearWidthThreshold = 4
maximumTopLeftCornerFoldTearHeightThreshold = 4
maximumBottomLeftCornerFoldTearWidthThreshold = 4
maximumBottomLeftCornerFoldTearHeightThreshold = 4
maximumTopRightCornerFoldTearWidthThreshold = 4
maximumTopRightCornerFoldTearHeightThreshold = 4
maximumBottomRightCornerFoldTearWidthThreshold = 4
maximumBottomRightCornerFoldTearHeightThreshold = 4

[MaskTable.defaultDefinition.doc.imageQuality.foldedOrTornDocumentEdges]
enable = Yes
maximumTopEdgeWidth = 5
maximumTopEdgeHeight = 5
maximumRightEdgeWidth = 5
maximumRightEdgeHeight = 5
maximumBottomEdgeWidth = 5
maximumBottomEdgeHeight = 5
maximumLeftEdgeWidth = 5
maximumLeftEdgeHeight = 5

[MaskTable.defaultDefinition.doc.imageQuality.documentFramingError]
enable = Yes
maximumAdditionalLeftScanLinesWidth = 5
maximumAdditionalTopScanLinesHeight = 5
maximumAdditionalRightScanLinesWidth = 5
maximumAdditionalBottomScanLinesHeight = 5

[MaskTable.defaultDefinition.doc.imageQuality.documentSkew]
enable = Yes
positiveDocumentSkewAngle = 30
negativeDocumentSkewAngle = -30

[MaskTable.defaultDefinition.doc.imageQuality.oversizeImage]
enable = Yes
maximumImageWidthThreshold = 90
maximumImageHeightThreshold = 45

[MaskTable.defaultDefinition.doc.imageQuality.piggybackDocument]
enable = Yes
threshold = 0.000000

[MaskTable.defaultDefinition.doc.imageQuality.imageTooLight]
enable = Yes
minimumPercentBlackPixels = 21
maximumPercentBrightness = 650
minimumPercentContrast = 259

[MaskTable.defaultDefinition.doc.imageQuality.imageTooDark]
enable = No
maximumPercentBlackPixels = 950
minimumPercentBrightness = 100

[MaskTable.defaultDefinition.doc.imageQuality.horizontalStreaks]
enable = No
maximumStreakHeightToBeDetectedThreshold = 0
maxBlackStreakCount = 0
maxBlackStreakMaxHeight = 0
blackStreakPercentageThreshold = 0
maxGrayLevelStreakCount = 0
maxGrayLevelStreakMaxHeight = 0
streakContrastThreshold = 0

[MaskTable.defaultDefinition.doc.imageQuality.belowMinimumCompressedFrontImageSize]
enable = No
minimumBitonalCompressedImageSize = 250
minimumGrayLevelCompressedImageSize = 250

[MaskTable.defaultDefinition.doc.imageQuality.aboveMaximumCompressedFrontImageSize]
enable = No
maximumBitonalCompressedImageSize = 200000
maximumGrayLevelCompressedImageSize = 200000

[MaskTable.defaultDefinition.doc.imageQuality.belowMinimumCompressedRearImageSize]
enable = No
minimumBitonalCompressedImageSize = 250
minimumGrayLevelCompressedImageSize = 250

[MaskTable.defaultDefinition.doc.imageQuality.aboveMaximumCompressedRearImageSize]
enable = No
maximumBitonalCompressedImageSize = 200000
maximumGrayLevelCompressedImageSize = 200000

[MaskTable.defaultDefinition.doc.imageQuality.spotNoise]
enable = Yes
maximumAverageSpotNoiseGroupingsPerSquareInch = 5852

[MaskTable.defaultDefinition.doc.imageQuality.frontRearImageDimensionMismatch]
enable = Yes
frontRearWidthDifference = 20
frontRearHeightDifference = 10

[MaskTable.defaultDefinition.doc.imageQuality.carbonStrip]
enable = Yes
minimumCarbonStripHeight = 3

[MaskTable.defaultDefinition.doc.imageQuality.outOfFocus]
enable = No
minimumImageFocusScore = 0.000000

[MaskTable.persistentData[1]]
Id = UserVocabularyPayee
type = Vocabulary
readyForRecognition = No

[MaskTable.persistentData[1].typeInfo.CaseVocabulary.vocabulary]
type = File
writeType = AutoDetect
handwritingStyle = Cursive
country = US

[MaskTable.persistentData[1].typeInfo.CaseVocabulary.vocabulary.typeInfo.CaseFile]
fileName = C:\Program Files(x86)\A2iA\A2iA CheckReader V9.0\Vocabularies\US\lastnames.dic
