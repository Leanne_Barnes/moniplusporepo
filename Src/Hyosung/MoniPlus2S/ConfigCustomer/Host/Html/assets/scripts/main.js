/**
 * Polyfill for Array.isArray method
 */
function isArray(arg) {
  return arg instanceof Array && arg.length !== 0;
}

/**
 * @param {*} obj 
 */
function isEmptyObject(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

/**
 * Assign value to $ symbol to avoid 'undefined' error.
 * https://stackoverflow.com/questions/32598971/whats-the-purpose-of-if-typeof-window-undefined
 * https://stackoverflow.com/questions/38746211/will-typeof-window-object-always-be-true
 */
var $ = typeof window === 'object' ? window.$ : {}

/**
 * Plays a single audio beep.
 * @remarks Not referenced within this file.
 */
function playBeep() {
  var pressed = {
    MessageType: 'TouchEventMessage',
    Command: 'Beep',
    FunctionKey: 'None'
  }

  sendMessage(pressed)
}

/**
 * Dictionary for keys/values that are shared across functions.
 * @todo Observed some different behavior in IE 11.
 */
var DataFrameWork = new Object()

/**
 * Dictionary for text resources that are shared across functions.
 * @todo Observed some different behavior in IE 11.
 */
var ResourceDictionary = new Object()

var ResourceData = new Object();

/**
 * Stores current application settings.
 */
function Settings() {
  this._currentLanguage = 'EN'
  /**
   * Supported languages are EN for English, SP for Spanish
   * and SO for Somalia.
   */
  this._supportedLanguages = ['EN', 'English']
  this._cssLanguageClasses = { EN: '', SP: 'spanish', SO: 'somali' }
}

/**
 * Removes any existing language class then adds the
 * current language class to the body of the document.
 */
Settings.prototype.updateLanguageDisplay = function updateLanguageDisplay() {
  if (this._currentLanguage) {
    if (this._cssLanguageClasses) {
      /**
       * Remove existing language class if it is not empty.
       */
      for (var key in this._cssLanguageClasses) {
        if (key) {
          var remover = this._cssLanguageClasses[key]
          if (remover) {
            var body = document.getElementsByTagName("body")[0];
            // removeClass(body, remover)
          }
        }
      }

      /**
       * Add the updated language css class.
       */
      var val = this._cssLanguageClasses[this._currentLanguage]
      if (val) {
        var body = document.getElementsByTagName("body")[0];
        // addClass(body, val);
      }
    }
  }
}

/**
 * INFO: indexOf polyfill 필요
 * Sets the current customer language.
 * @param {string} lang The two character language.
 * @description EN - English, SP - Spanish, SO - Somalia
 */
Settings.prototype.setLanguage = function setLanguage(lang) {
  if (lang) {
    if (this._supportedLanguages.indexOf(lang) !== -1) {
      this._currentLanguage = lang
      this.updateLanguageDisplay()
    } else {
      log('Unsupported language value of ' + lang + '.')
    }
  }
}

/**
 * Instance for the current application settings.
 */
var CurrentTransaction = new Settings()

/**
 * Constant values for string literals.
 */
var CONSTANTS = {
  DATAFRAMEWORK: 'dataFramework',
  RESOURCEDICTIONARY: 'resourceDictionary',
  TEXT_DATAFRAMEWORK: 'text-dataFramework',
  REPLACEMENT_CHAR: '####',
  AVAILABLE_BALANCE: 'AvailableBalance',
  MAXIMUM_WITHDRAWAL_AMOUNT: 'MaximumWithdrawalAmount',
}

/**
 * Provides methods for converting between JS objects and JSON types.
 */
function JsonConvert() { }

/**
 * Deserializes the JSON to an object.
 *
 * @param {string} entity The JSON string to be converted.
 * @return {object} The deserialized object from the JSON string.
 */
JsonConvert.prototype.DeserializeObject = function DeserializeObject(entity) {
  var result = {}
  try {
    result = JSON.parse(entity)
  } catch (error) {
    result = {}
  }

  if (typeof result !== 'object') {
    result = {}
  }

  return result
}

/**
 * Serializes the specified object to a JSON string.
 * @param {object} entity The object to be converted.
 * @return {string} A JSON string representation of the object.
 */
JsonConvert.prototype.SerializeObject = function SerializeObject(entity) {
  var result = ''
  try {
    result = JSON.stringify(entity)
  } catch (error) {
    result = ''
  }

  if (typeof result !== 'string') {
    result = ''
  }

  return result
}

/**
 * @function sendMessageArray
 * @description Sends the array of messages as an individual message.
 *
 * @example [{"MessageType":"DataFrameWorkMessage","Command":"Get","Section":"CurrentTransaction","Key":"ValidAccounts"}, ...]
 */
function sendMessageArray(messages) {
  if (messages) {
    if (isArray(message)) {
      for (var x = 0; x < messages.length; x += 1) {
        sendMessage(messages[x])
      }
    } else {
      sendMessage(messages)
    }
  }
}

/**
 *  Send message to C# application as JSON string.
 *
 *  @param {object} message JSON object to send to C# application.
 *  @return {boolean} True if no error otherwise false.
 */
function sendMessage(message) {
  if (isEmptyObject(message)) {
    return false
  }

  if (!window.external) {
    return false
  }

  var result = true
  try {
    var convert = new JsonConvert()
    var data = convert.SerializeObject(message)
    if (data) {
      window.external.DataRequested(data)
    }
  } catch (error) {
    result = false
  }
  return result
}

/**
 * Writes to the log if the log exists.
 *
 * @param {*} message Content to be written to the log.
 */
function log(message) {
  if (typeof message === 'string') {
    if (window.console) {
      console.log(message)
    }

    var data = {
      MessageType: 'LogMessage',
      Command: 'Info',
      Contents: message
    }

    sendMessage(data)
  }
}

/**
 * INFO: pageParseImage가 구현되지 않음
 * @param {*} data 
 */
function parseImageReplyMessage(data) {
  if (data) {
    if (typeof pageParseImage === 'function') {
      // INFO: 미구현된 함수 호출부 주석처리
      // pageParseImage(data)
    }
  }
}

/**
 * Sets the page elements text whose id starts with 'text-dataFramework-' to
 * the value in the object parameter.
 *
 * @param {*} data
 * @example {"MessageReplyType":"DataFrameWorkReply","Status":"Good","Section":"System","Key":"MachineNumber","Value":"HN1234"}
 */
function parseDataMessage(data) {
  if (data) {
    var val = Object.prototype.hasOwnProperty.call(data, 'Value') ? data.Value : ''
    var key = Object.prototype.hasOwnProperty.call(data, 'Key') ? data.Key : ''
    var section = Object.prototype.hasOwnProperty.call(data, 'Section') ? data.Section : ''
    if (val && key) {
      var unique = section + '.' + key
      DataFrameWork[unique] = val
      var id = CONSTANTS.TEXT_DATAFRAMEWORK + '-' + section + '.' + key
      if (key === CONSTANTS.AVAILABLE_BALANCE || key === CONSTANTS.MAXIMUM_WITHDRAWAL_AMOUNT) val = "GBP" + val;
      document.getElementById(id).innerHTML = val
    }
  } else {
    log('Parameter is null or undefined.')
  }
}

/**
 * Sets the page elements text whose id starts with 'text-resourceDictionary-' to
 * the value in the object parameter.
 *
 * @param {*} data
 * @example {"MessageType":"ResourceDictionaryMessage","Command":"Get","Keys":[{"HtmlElementId":"text-resourceDictionary-Reserved.Offline","StringResourceId":"Reserved.Offline"}],"Language":"EN"}.
 */
function parseResourceMessage(data) {
  if (data) {
    var values = Object.prototype.hasOwnProperty.call(data, 'Values') ? data.Values : ''
    if (values.length > 0) {
      for (var x = 0; x < values.length; x += 1) {
        var elementId = values[x].HtmlElementId

        var val = values[x].Value
        var id = elementId

        var text = document.getElementById(id).innerHTML

        var stored = ResourceDictionary[id]
        if (!stored) {
          ResourceDictionary[id] = text
          stored = text
        }

        var markup = ''
        if (stored) {
          if (stored.indexOf(CONSTANTS.REPLACEMENT_CHAR) !== -1) {
            markup = stored.replace(CONSTANTS.REPLACEMENT_CHAR, val)
          } else {
            markup = val
          }
        } else {
          markup = val
        }

        document.getElementById(id).innerHTML = markup
      }
    }
  }
}

/**
 * Parses the reply from the application heartbeat message.
 * Sets the current page language.
 *
 * @param {*} data
 * @example {"Section":"HeartBeat","MessageReplyType":"HeartBeatReply","Language":"EN","Status":"Good"}.
 */
function parseHeartBeatMessage(data) {
  // TODO: toggleClass jQuery 제거해야 함
  // if (data) {
  //   const lang = Object.prototype.hasOwnProperty.call(data, 'Language') ? data.Language : ''
  //   if (lang) {
  //     CurrentTransaction.setLanguage(lang)
  //     const id = '#key-language-' + CurrentTransaction._currentLanguage
  //     $(id).toggleClass('pressed')
  //     requestScreenText(CONSTANTS.RESOURCEDICTIONARY)
  //   }
  // }
}

/**
 *  Process messages received from the C# application.
 *
 * @param {string} message JSON string received from C# applicaiton.
 */
function onMessage(message) {
  if (message) {
    if (typeof message === 'string') {
      var converter = new JsonConvert()
      var data = converter.DeserializeObject(message)

      if (isEmptyObject(data)) {
        log('Error object is empty.')
        return
      }

      var type = Object.prototype.hasOwnProperty.call(data, 'MessageReplyType') ? data.MessageReplyType : ''

      if (type === '') {
        log('MessageReplyType is an empty string.')
        return
      }

      switch (type) {
        case 'DataFrameWorkReply':
          parseDataMessage(data)
          break
        case 'ResourceDictionaryReply':
          parseResourceMessage(data)
          break
        case 'HeartBeatReply':
          parseHeartBeatMessage(data)
          break
        case 'ImageRequestReply':
          parseImageReplyMessage(data)
          break
        default:
          log('Unhandled message type.')
      }

      if (typeof pageOnMessage === 'function') {
        pageOnMessage(data)
      }
    }
  }
}

/**
 * Returns an object with the correct selector for the event.
 *
 * @param {*} id
 * @returns {object} { selector: string, clickEnabled: boolean }
 */
function getMouseEventSender(id, source) {
  var errorResult = { selector: '', clickEnabled: false }
  var lookup = source || '^click-'

  if (!id || isEmptyObject(id)) {
    return errorResult
  }

  var failed = false
  var item = ''
  var isClickEnabled = false

  try {
    var find = new RegExp(lookup, 'i')
    itemId = '#' + id
    isClickEnabled = find.test(id)

    if (!isClickEnabled) {
      var parentId = document.getElementById(itemId).parentNode.id
      isClickEnabled = find.test(parentId)
      item = isClickEnabled ? '#' + parentId : item
    }
  } catch (error) {
    failed = true
  }

  if (failed) {
    return errorResult
  }
  return { selector: item, clickEnabled: isClickEnabled }
}

function isStartsWith(search, str) {
  return str.substring(0, search.length) === search;
}

function getButtonsByIdStartingWith(s) {
  var buttons = []
  var divs = document.getElementsByTagName("div")
  for (var i=0; i < divs.length; i++) {
    var div = divs[i]
    if (isStartsWith(s, div.id)) {
      buttons.push(div)
    }
  }
  return buttons
}

function getClosestButton(el, s) {
  if (isStartsWith(s, el.id)) {
    return el
  }
  return el.parentElement
}

function addMouseDownEvent() {
  var buttons = getButtonsByIdStartingWith("click-")
  for (var i=0; i < buttons.length; i++) {
    buttons[i].attachEvent('onmousedown', onButtonDown)
  }
}

function removeMouseDownEvent() {
  var buttons = getButtonsByIdStartingWith("click-")
  for (var i=0; i < buttons.length; i++) {
    buttons[i].detachEvent('onmousedown', onButtonDown)
  }
}

function addMouseUpEvent() {
  var buttons = getButtonsByIdStartingWith("click-")
  for (var i=0; i < buttons.length; i++) {
    buttons[i].attachEvent('onmouseup', onButtonUp)
  }
}

function removeMouseUpEvent() {
  var buttons = getButtonsByIdStartingWith("click-")
  for (var i=0; i < buttons.length; i++) {
    buttons[i].detachEvent('onmouseup', onButtonUp)
  }
}

function addMouseLeaveEvent() {
  var buttons = getButtonsByIdStartingWith("click-")
  for (var i=0; i < buttons.length; i++) {
    buttons[i].attachEvent('onmouseleave', onButtonUp)
  }
}

/**
 * INFO: 버튼 UI toggle은 생략
 * Changes the button visual state when the event is raised.
 * Removes the event 'mousedown' from all buttons.
 *
 * @param {*} evt
 */
function onButtonDown(evt) {
  if (evt && evt.srcElement) {
    var id = getClosestButton(evt.srcElement, "click-").id
    if (id) {
      log('[main.js] onButtonDown() id: ' + id)
      removeMouseDownEvent()
    }
  }
}

function updateLanguage(id) {
  var splitted = id.split('-')
  if (splitted.length === 3) {
    CurrentTransaction.setLanguage(splitted[2])
    requestScreenText(CONSTANTS.RESOURCEDICTIONARY)
  }
}

/**
 * INFO: UI 토글은 생략함
 * Changes the language used for the screen.
 * 
 * @param {*} evt
 */
function onLanguageDown(evt) {
  if (evt && evt.srcElement) {
    var id = getClosestButton(evt.srcElement, "key-language-").id
    if (id) {
      playBeep()
      updateLanguage(id)
    }
  }
}

function sendButtonPressedEvent(id) {
  var splitted = id.split('-')
  if (splitted.length === 3) {
    var selectedKey = splitted[2]
    var pressed = {
      MessageType: 'TouchEventMessage',
      Command: 'Pressed',
      FunctionKey: selectedKey
    }

    sendMessage(pressed)
  }
}

/**
 * INFO: 버튼 UI toggle은 생략
 * Changes the button visual state when the event is raised.
 * Removes the event 'mouseup mouseleave' from all buttons.
 *
 * @param {*} evt
 */
function onButtonUp(evt) {
  if (evt && evt.srcElement) {
    var id = getClosestButton(evt.srcElement, "click-").id
    if (id) {
      removeMouseUpEvent()
      sendButtonPressedEvent(id)
    }
  }
}

/**
 * Rotates an array of images when an elment id of 'advertising' exists.
 * @todo Not final implementation.
 */
function startAdvertisment() {
  var ad = document.getElementById('advertising');
  if (ad !== null) {
    var index = 1
    setInterval(function () {
      var images = [
        'url(assets/img/welcome1.PNG)',
        'url(assets/img/welcome2.PNG)',
        'url(assets/img/welcome3.PNG)',
      ];
      ad.style.backgroundImage = images[index];
      index += 1;
      if (index > 2) index = 0
    }, 10000);
  }
}

function startAdvertismentVG() {
  var ad = document.getElementById('vg-advertising');
  if (ad !== null) {
    var index = 1
    setInterval(function () {
      var images = [
        'url(assets/img/VGwelcome1.PNG)',
        'url(assets/img/VGwelcome2.PNG)',
        'url(assets/img/welcome3.PNG)',
      ];
      ad.style.backgroundImage = images[index];
      index += 1;
      if (index > 2) index = 0
    }, 10000);
  }
}

/**
 * Add button event handlers to element ids that start with 'click-' or 'key-language-'.
 */
function attachButtonHandlers() {
  // removeMouseDownEvent()
  // removeMouseUpEvent()
  addMouseDownEvent()
  addMouseUpEvent()
}

/**
 * Setup anything after the page has executed it functions.
 */
function completePage() {
  log('Callback function completePage has been called.')
}

/**
 * Setup anything that is needed for the page.
 */
function initPage() {
  try {
    startAdvertisment();
    startAdvertismentVG();
    attachButtonHandlers();

    if (typeof pageLoader === 'function') {
      pageLoader(completePage)
    } else {
      completePage()
    }
  } catch (error) {
    log(error.message)
    log('Failed to initialize the page.')
  }
}

function updateSelector(selector) {
  var messages = []
  for (var x = 0; x < selector.length; x += 1) {
    var elementId = selector[x].id;

    if (typeof elementId === 'string') {
      var parts = elementId.split('-')

      if (parts.length === 3) {
        var source = parts[1]
        var key = parts[2]
        var message = {}

        // Example 'text-resourceDictionary-MainMenu.Withdraw'
        // Pattern 'text-source-key'
        // Get the value from the 'source' using the 'key'.
        if (source === CONSTANTS.RESOURCEDICTIONARY) {
          message = {
            MessageType: 'ResourceDictionaryMessage',
            Command: 'Get',
            Keys: [{ HtmlElementId: elementId, StringResourceId: parts[2] }],
            Language: CurrentTransaction._currentLanguage
          }
        } else if (source === CONSTANTS.DATAFRAMEWORK) {
          var data = key.split('.')
          message = {
            MessageType: 'DataFrameWorkMessage',
            Command: 'Get',
            Section: data[0],
            Key: data[1]
          }
        }

        if (!isEmptyObject(message)) {
          messages.push(message)
        }
      }
    }
  }
  return messages
}

/**
 * Update the screen display text.
 */
function updateScreenText(lookup) {
  var messages = []
  var selector1 = null;
  var selector2 = null;
  var selector3 = null;
  var selector4 = null;
  var selector5 = null;

  selector1 = document.getElementsByTagName('div')
  selector2 = document.getElementsByTagName('h2')
  selector3 = document.getElementsByTagName('h3')
  selector4 = document.getElementsByTagName('p')
  selector5 = document.getElementsByTagName('span')

  messages = updateSelector(selector1)
  messages = messages.concat(updateSelector(selector2))
  messages = messages.concat(updateSelector(selector3))
  messages = messages.concat(updateSelector(selector4))
  messages = messages.concat(updateSelector(selector5))
  for (var msg in messages) {
    log('[main.js] msg: ' + messages[msg])
  }

  //if (selector === null || selector.length === 0) {
  //    log('No id\'s found for the query.')
  //}    
  /**
   * Loop through the found elements.
   *
   * jQuery.each()
   * https://api.jquery.com/each/#each-function
   *
   */

  //for (var x = 0; x < selector.length; x += 1)
  //{
  //    var elementId = selector[x].id;

  //        if (typeof elementId === 'string') {
  //        var parts = elementId.split('-')

  //        if (parts.length === 3) {
  //            var source = parts[1]
  //            var key = parts[2]
  //            var message = {}

  //            // Example 'text-resourceDictionary-MainMenu.Withdraw'
  //            // Pattern 'text-source-key'
  //            // Get the value from the 'source' using the 'key'.
  //            if (source === CONSTANTS.RESOURCEDICTIONARY) {
  //                message = {
  //                    MessageType: 'ResourceDictionaryMessage',
  //                    Command: 'Get',
  //                    Keys: [{ HtmlElementId: elementId, StringResourceId: parts[2] }],
  //                    Language: CurrentTransaction._currentLanguage
  //                }
  //            } else if (source === CONSTANTS.DATAFRAMEWORK) {
  //                var data = key.split('.')
  //                message = {
  //                    MessageType: 'DataFrameWorkMessage',
  //                    Command: 'Get',
  //                    Section: data[0],
  //                    Key: data[1]
  //                }
  //            }

  //            if (!isEmptyObject(message)) {
  //                messages.push(message)
  //            }
  //        }
  //    }
  //}
  //selector.each(function () {
  //    // Get the id of the element in this iteration.
  //    var elementId = $(this).attr('id')

  //    if (typeof elementId === 'string') {
  //        var parts = elementId.split('-')

  //        if (parts.length === 3) {
  //            var source = parts[1]
  //            var key = parts[2]
  //            var message = {}

  //            // Example 'text-resourceDictionary-MainMenu.Withdraw'
  //            // Pattern 'text-source-key'
  //            // Get the value from the 'source' using the 'key'.
  //            if (source === CONSTANTS.RESOURCEDICTIONARY) {
  //                message = {
  //                    MessageType: 'ResourceDictionaryMessage',
  //                    Command: 'Get',
  //                    Keys: [{ HtmlElementId: elementId, StringResourceId: parts[2] }],
  //                    Language: CurrentTransaction._currentLanguage
  //                }
  //            } else if (source === CONSTANTS.DATAFRAMEWORK) {
  //                var data = key.split('.')
  //                message = {
  //                    MessageType: 'DataFrameWorkMessage',
  //                    Command: 'Get',
  //                    Section: data[0],
  //                    Key: data[1]
  //                }
  //            }

  //            if (!isEmptyObject(message)) {
  //                messages.push(message)
  //            }
  //        }
  //    }
  //})

  return messages
}

/**
 * TODO: indexOf polyfill 필요
 * Send the heart beat message to the C# application.
 * @example {"MessageType":"HeartBeatMessage","FileName":"Example.html"}.
 */
function sendHeartBeat() {
  var result = { MessageType: 'HeartBeatMessage', FileName: 'UnKnown' }
  var name = ''
  if (document.location.href) {
    var url = document.location.href
    if (url && url.length > 0) {
      try {
        url = url.substring(0, (url.indexOf('#') === -1) ? url.length : url.indexOf('#'))
        url = url.substring(0, (url.indexOf('?') === -1) ? url.length : url.indexOf('?'))
        url = url.substring(url.lastIndexOf('/') + 1, url.length)
        name = url
      } catch (error) {
        name = ''
      }
    }
  }

  if (name !== '') {
    result.FileName = name
  }

  if (result) {
    sendMessage(result)
  }
}

/**
 * Get the screen text from the data framework or resourcedictionary.
 *
 * @param {string} lookup
 */
function requestScreenText(lookup) {
  if (lookup) {
    var dataMessage = updateScreenText(lookup)
    if (isArray(dataMessage)) {
      for (var x = 0; x < dataMessage.length; x += 1) {
        sendMessage(dataMessage[x])
      }
    }
  }
}

/**
 * Application entry point.
 */
function loader() {
  if (window) {
    window.detachEvent('onload', loader, false)

    if (typeof sendHeartBeat === 'function') {
      sendHeartBeat()
    }

    if (typeof pageRequestData === 'function') {
      pageRequestData()
    }

    if (typeof requestScreenText === 'function') {
      requestScreenText(CONSTANTS.DATAFRAMEWORK)
    }

    if (typeof initPage === 'function') {
      initPage()
    }
  }
}

/**
 * Attach handler for the window onerror event.
 * Attach handler for the window load event.
 * 전역 onerror 이벤트 핸들러
 * onerror의 파라미터 중 message, URL, lineNo만 IE8에서 지원되고, colNo와 errorObj는 지원되지 않음
 */
if (typeof window === 'object') {
  window.onerror = function (msg, url, line) {
    try {
      var fileName = 'NONE'
      if (url) {
        fileName = url.replace(/^.*[\\/]/, '')
      }

      var message = 'NONE'
      if (msg) {
        message = msg
      }

      var lineNumber = 'NONE'
      if (line) {
        lineNumber = line
      }

      var errorMessage = 'JS Error.  File : [' + fileName + '] Line : [' + lineNumber + '] Description : [' + message + '].'
      log(errorMessage)
    } catch (error) {
      log('Error while creating message on window.onerror.')
    }
    return true
  }

  window.onload = function () {
    if (typeof sendHeartBeat === 'function') {
      sendHeartBeat()
    }

    if (typeof pageRequestData === 'function') {
      pageRequestData()
    }

    if (typeof requestScreenText === 'function') {
      requestScreenText(CONSTANTS.DATAFRAMEWORK)
    }

    if (typeof initPage === 'function') {
      initPage()
    }
  }
  window.attachEvent('onload', loader)
}
