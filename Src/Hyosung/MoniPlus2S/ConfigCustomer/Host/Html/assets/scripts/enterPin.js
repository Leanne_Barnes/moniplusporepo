var inputCount = 0;
var inputData = '';
var MAX_PIN_LENGTH = 6;

function pageOnMessage(data) {
  if (data) {
    var device = Object.prototype.hasOwnProperty.call(data, 'Device') ? data.Device : '';
    var val = Object.prototype.hasOwnProperty.call(data, 'Pressed') ? data.Pressed : '';

    log('[enterPin.js] KeyPressed => key: ' + val)

    if (device && val) {
      if (device === 'PinPad') {
        if (val !== 'ENTER' && val !== 'CANCEL') {
          if (val === 'CLEAR') {
            inputCount = 0;
            inputData = '';
          } else if (inputCount < MAX_PIN_LENGTH) {
            inputCount += 1;
            inputData += '*';
          }

          document.getElementById('pin-data').innerHTML = inputData;
        }
      }
    }
  }
}