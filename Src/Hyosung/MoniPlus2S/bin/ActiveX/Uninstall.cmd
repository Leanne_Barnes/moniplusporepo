cd /d %~dp0

rem *** Always unregister the file first

regsvr32 /s /u "AxFingerNW.ocx"
regsvr32 /s /u "AxSPL.ocx"
regsvr32 /s /u "AxVFD.ocx"
regsvr32 /s /u "CDWriterXP.ocx"
regsvr32 /s /u "JiRoBackUp.ocx"
regsvr32 /s /u "MSCOMM32.ocx"
regsvr32 /s /u "NDCDialUp.ocx"
regsvr32 /s /u "NHMwiEmv.ocx"
regsvr32 /s /u "NHTiltController.ocx"
regsvr32 /s /u "NHXComm.ocx"
