cd /d %~dp0

if exist "c:\xfs" (goto INSTALL) ELSE (goto EXIT)

:INSTALL
set forms=c:\xfs\data\urptr

rem *** Note: NCR variable will probably evaluate to: c:\xfs\data\urptr

for /f "delims=" %%i in ('dir /ad/b') do (
  xcopy /R /F /Y "%%i" "%forms%\%%i\"
)

:EXIT
