cd /d %~dp0

set forms=C:\Program Files\Diebold\AgilisXFS\SampleForm

rem *** Note: Diebold variable will probably evaluate to: C:\Program Files\Diebold\AgilisXFS\SampleForm

for /f "delims=" %%i in ('dir /ad/b') do (
  xcopy /R /F /Y "%%i" "%forms%\%%i\"
)
