cd /d %~dp0

if exist "C:\Program Files\Diebold" (goto INSTALL) ELSE (goto EXIT)

:INSTALL
set forms=C:\Program Files\Diebold\AgilisXFS

rem *** Note: Diebold variable will probably evaluate to: C:\Program Files\Diebold\AgilisXFS

for /f "delims=" %%i in ('dir /ad/b') do (
  xcopy /R /F /Y "%%i" "%forms%\%%i\"
)

:EXIT
