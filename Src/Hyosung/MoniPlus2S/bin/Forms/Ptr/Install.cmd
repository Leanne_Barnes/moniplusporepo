cd /d %~dp0

if exist "C:\ProBase" (goto INSTALL) ELSE (goto EXIT)

:INSTALL
set forms=C:\ProBase\ProDevice\Forms\Ptr

rem *** Note: Wincor variable will probably evaluate to: C:\ProBase\ProDevice\Forms\Ptr

for /f "delims=" %%i in ('dir /b') do (
  xcopy /R /F /Y "%%i" "%forms%\"
)

:EXIT
