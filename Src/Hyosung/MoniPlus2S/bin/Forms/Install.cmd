cd /d %~dp0

rem *** If Nextware isn't installed yet then use legacy default folder
set forms=C:\Program Files\Nextware\Form

rem *** If Nextware isn't installed yet then determine default location for Windows 10
for /f "tokens=4-5 delims=. " %%i in ('ver') do set version=%%i.%%j
if "%version%" == "10.0" set forms=C:\Hyosung\Nextware\Form

rem *** Nextware should be installed so determine which folder it is in.
rem *** Note: On Windows 7 (32bit) Nextware variable should evaluate to: C:\Program Files\Nextware\EXE
rem *** Note: On Windows 7 (64bit) Nextware variable should evaluate to: C:\Program Files (x86)\Nextware\EXE
rem *** Note: On Windows 10 Nextware variable should evaluate to: C:\Hyosung\Nextware\EXE
if "%Nextware%" NEQ "" set forms=%Nextware:~0,-3%Form

rem *** Copy all the Form Files
for /f "delims=" %%i in ('dir /ad/b') do (
  xcopy /R /F /Y "%%i" "%forms%\%%i\30\"
)

rem *** Execute any possible Install.cmd files
for /f "delims=" %%i in ('dir /ad/b') do (
  if exist "%%i\Install.cmd" call "%%i\Install.cmd"
  cd /d "%cd%"
)

rem *** Replace single \ with double \\
rem *** For example C:\Hyosung\Nextware\Form -> C:\\Hyosung\\Nextware\\Form
set replaceString=%forms:\=\\%

rem *** Make sure any absolute paths in wfm files correctly reference that actual install location
for /R "%forms%" %%f in (*.wfm) do (
  cscript.exe /Nologo replace.vbs "%%f" "C:\\Program Files\\Nextware\\Form" "%replaceString%"
)
