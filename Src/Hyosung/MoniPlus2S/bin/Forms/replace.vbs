'Usage: cscript replace.vbs "Filename" "StringToFind" "stringToReplace"
 
Option Explicit
Dim fso, strFilename, strSearch, strReplace, objFile, oldContent, newContent
 
strFilename=WScript.Arguments.Item(0)
strSearch=WScript.Arguments.Item(1)
strReplace=WScript.Arguments.Item(2)
 
'Does file exist?
Set fso=CreateObject("Scripting.FileSystemObject")
if fso.FileExists(strFilename)=false then
	wscript.echo "File not found!"
	wscript.Quit
end if
 
set objFile=fso.OpenTextFile(strFilename, 1)

'Make sure the file is not 0 bytes or an error will be thrown
if not objFile.AtEndOfStream then

	'Read file
	oldContent=objFile.ReadAll

	'Make sure the string to be replaced exists in this particular file
	if (InStr(oldContent, strSearch)) then
	
		'Replace the string
		newContent=replace(oldContent, strSearch, strReplace, 1, -1, 0)
		
		'Make sure the file is opened in Unicode mode or error will be thrown
		set objFile=fso.OpenTextFile(strFilename, 2, -1)
		
		'Write out updated file
		objFile.Write newContent
	
	end if
	
end if

objFile.Close 
