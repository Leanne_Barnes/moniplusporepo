cd /d %~dp0

for /f "delims=" %%i in ('dir /ad/b') do (
  if exist "%%i\Install.cmd" call "%%i\Install.cmd" %1 %2 %3 %4 %5
  cd /d "%~dp0"
)

if exist "InstallService.cmd" (
  call "InstallService.cmd"
)
