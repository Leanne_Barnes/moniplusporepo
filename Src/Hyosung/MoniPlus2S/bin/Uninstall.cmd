cd /d %~dp0

for /f "delims=" %%i in ('dir /ad/b') do (
  if exist "%%i\Uninstall.cmd" call "%%i\Uninstall.cmd %1 %2 %3 %4 %5"
  cd /d "%~dp0"
)

if exist "UninstallService.cmd" (
  call "UninstallService.cmd"
)
